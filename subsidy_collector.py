#!/usr/bin/env python
# -*- coding: utf-8 -*-

from tkinter import font
from tkinter import messagebox
from tkinter import filedialog
import tkinter as tk
import os, sys
from datetime import datetime
from subsidy import driver

class Application(tk.Frame):
    def __init__(self, master = None):
        super().__init__(master)
        self.pack()
        self.create_widgets()

    def create_widgets(self):
        self.frame = tk.Frame(width = 700, height = 530)
        self.frame.pack()
        
        logo = tk.PhotoImage(file = 'logo_large.png')
        self.lab = tk.Label(self.frame, image = logo)
        self.lab.image = logo
        self.lab.pack()

        self.header = tk.Label(self.frame,
                text = 'Subsidy Collector',
                padx = 5, pady = 5,
                font = font.Font(family = 'Helvetica',
                    size = 24, weight = 'bold'),
                fg = '#0000FF',
                )
        self.header.pack()

        self.version = tk.Label(self.frame,
                text = 'version 0.0.1',
                font = font.Font(family = 'Helvetica',
                    size = 10),
                fg = '#A0A0A0',
                )
        self.version.pack()

        self.copyright = tk.Label(self.frame,
                text = 'Zhang Shaowei<z_sw@hotmail.com>',
                font = font.Font(slant = 'italic', size = 10),
                fg = '#A0A0A0',
                )
        self.copyright.pack()

        self.input = tk.LabelFrame(self.frame, text = '选择统计参数', width = 400, height = 300)
        self.input.pack()

        self.lab1 = tk.Label(self.input, text = '统计项目:')
        self.lab1.grid(row = 0, column = 0, sticky = tk.W, padx = 5, pady = 5)
        
        self.project = tk.StringVar()
        self.project_entry = tk.Entry(self.input, textvariable = self.project)
        self.project_entry.grid(row = 0, column = 1, sticky = tk.W, padx = 5, pady = 5)

        self.memo1 = tk.Label(self.input, text = '填写要统计的基础项目')
        self.memo1.grid(row = 0, column = 2, sticky = tk.W, padx = 5, pady = 5)

        self.lab2 = tk.Label(self.input, text = '合并项目:')
        self.lab2.grid(row = 1, column = 0, sticky = tk.W, padx = 5, pady = 5)

        self.merging = tk.StringVar()
        self.merging = tk.Entry(self.input, textvariable = self.merging)
        self.merging.grid(row = 1, column = 1, sticky = tk.W, padx = 5, pady = 5)

        self.memo2 = tk.Label(self.input, text = '以空格分隔要合并统计的其它项目(可选)')
        self.memo2.grid(row = 1, column = 2, sticky = tk.W, padx = 5, pady = 5)

        self.lab3 = tk.Label(self.input, text = '统计年份:')
        self.lab3.grid(row = 2, column = 0, sticky = tk.W, padx = 5, pady = 5)
        
        self.year = tk.IntVar()
        self.year.set(datetime.today().year)
        self.year_spinbox = tk.Spinbox(self.input,
                from_ = datetime.today().year - 1,
                to = datetime.today().year + 1,
                textvariable = self.year
                )
        self.year_spinbox.grid(row = 2, column = 1, sticky = tk.W, padx = 5, pady = 5)

        self.memo3 = tk.Label(self.input, text = '填写统计的年份')
        self.memo3.grid(row = 2, column = 2, sticky = tk.W, padx = 5, pady = 5)

        self.lab4 = tk.Label(self.input, text = '统计月份:')
        self.lab4.grid(row = 3, column = 0, sticky = tk.W, padx = 5, pady = 5)
        
        self.month = tk.IntVar()
        self.month.set(datetime.today().month)
        self.month_spinbox = tk.Spinbox(self.input, from_ = 1, to = 12, textvariable = self.month)
        self.month_spinbox.grid(row = 3, column = 1, sticky = tk.W, padx = 5, pady = 5)

        self.memo4 = tk.Label(self.input, text = '填写统计的月份')
        self.memo4.grid(row = 3, column = 2, sticky = tk.W, padx = 5, pady = 5)

        self.input_dir_labelframe = tk.LabelFrame(self.input, text = '待统计表格存放目录', width = 600, height = 300)
        self.input_dir_labelframe.grid(row = 4, column = 0, columnspan = 3, rowspan = 3, sticky = tk.W, padx = 5, pady = 5)
        
        self.input_dir = tk.StringVar()
        self.input_dir_entry = tk.Entry(self.input_dir_labelframe, width = 60, textvariable = self.input_dir)
        self.input_dir_entry.grid(row = 0, column = 0, columnspan = 2, sticky = tk.W, padx = 5, pady = 1)

        self.input_dir_browse = tk.Button(self.input_dir_labelframe, text = '浏览', command = self.get_input_dir)
        self.input_dir_browse.grid(row = 0, column = 3,  sticky = tk.W, padx = 5, pady = 1)

        self.output_dir_labelframe = tk.LabelFrame(self.input, text = '统计或模板输出目录', width = 600, height = 300)
        self.output_dir_labelframe.grid(row = 7, column = 0, columnspan = 3, rowspan = 3, sticky = tk.W, padx = 5, pady = 5)
        
        self.output_dir = tk.StringVar()
        self.output_dir_entry = tk.Entry(self.output_dir_labelframe, width = 60, textvariable = self.output_dir)
        self.output_dir_entry.grid(row = 0, column = 0, columnspan = 2, sticky = tk.W, padx = 5, pady = 1)

        self.output_dir_browse = tk.Button(self.output_dir_labelframe, text = '浏览', command = self.get_output_dir)
        self.output_dir_browse.grid(row = 0, column = 3,  sticky = tk.W, padx = 5, pady = 1)

        self.footer = tk.Frame(self.frame)
        self.footer.pack()
        self.get_template = tk.Button(self.footer, text = '生成模板',
                              command = self.get_template)
        self.get_template.grid(row = 0, column = 0, sticky = tk.W, padx = 50, pady = 15)

        self.btn1 = tk.Button(self.footer, text = '统计数据',
                              command = self.get_subsidy)
        self.btn1.grid(row = 0, column = 1, padx = 50, pady = 15)
        
        self.quit = tk.Button(self.footer, text = '退出系统', command = window.destroy)
        self.quit.grid(row = 0, column = 2, sticky = tk.E, padx = 50, pady = 15)

    def get_input_dir(self):
        v = filedialog.askdirectory()
        if v:
            self.input_dir.set(v)

    def get_output_dir(self):
        v = filedialog.askdirectory()
        if v:
            self.output_dir.set(v)

    def get_subsidy(self):
        try:
            prj = self.project.get().strip()
            me = self.merging.get().strip().split(' ')
            i = self.input_dir.get().strip()
            o = self.output_dir.get().strip()
            ym = '{}{}'.format(str(self.year.get()), str(self.month.get()).zfill(2))
            
            if prj.count(' '):
                messagebox.showerror('错误', '项目名称中包含空格:{}'.format(prj))
                return
            
            if not os.path.exists(i):
                messagebox.showerror('错误', '待统计表格存放目录"{}"不存在'.format(i))
                return

            if not os.path.exists(o):
                messagebox.showerror('错误', '统计或模板输出目录"{}"不存在'.format(o))
                return
            
            driver.get_subsidy(o, i, ym, prj, me)
            messagebox.showinfo('信息',
                    '对"{}"统计保存为文件"{}"'
                                .format(i, os.path.join(o, prj + '_' + ym + '.xlsx')))
            
        except Exception as e:
            messagebox.showerror('错误', e)

    def get_template(self):
        try:
            o = self.output_dir.get().strip()
            if not os.path.exists(o):
                messagebox.showerror('错误', '统计或模板输出目录"{}"不存在'.format(o))
                return
            driver.get_template(o)
            messagebox.showinfo('信息',
                    '已在"{}"中保存了模板文件"工时统计_NAME_YYYYMM.xlsm"'
                    .format(o))
        except Exception as e:
            messagebox.showerror('错误', e)

if __name__ == '__main__':
    window = tk.Tk()
    window.geometry('620x580')
    app = Application(master = window)
    app.master.title('工时统计')
    if str.startswith(sys.platform, 'win'):
        app.master.iconbitmap('app.ico')
    app.mainloop()

