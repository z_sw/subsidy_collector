#!/usr/bin/env python
# -*- coding: utf-8 -*-

from cx_Freeze import setup, Executable
import os.path
import sys

PYTHON_INSTALL_DIR = os.path.dirname(os.path.dirname(os.__file__))
os.environ['TCL_LIBRARY'] = os.path.join(PYTHON_INSTALL_DIR, 'tcl', 'tcl8.6')
os.environ['TK_LIBRARY'] = os.path.join(PYTHON_INSTALL_DIR, 'tcl', 'tk8.6')

buildOptions = dict(
        packages = [
            'tkinter',
            'pathlib',
            'xlsxwriter',
            'xlrd',],
        excludes = ['subsidy/__pycache__'],
        include_files = [
            'vbaProject.bin',
            'logo.png',
            'logo_large.png',
            'Readme.md',
            'subsidy',
            'app.ico',
            ],
        )

base = None
if sys.platform == 'win32':
    base = 'Win32GUI'
    buildOptions['include_files'].extend([
            os.path.join(PYTHON_INSTALL_DIR, 'DLLs', 'tk86t.dll'),
            os.path.join(PYTHON_INSTALL_DIR, 'DLLs', 'tcl86t.dll'),
            ])

executables = [
    Executable('subsidy_collector.py', base=base,
        icon='app.ico',
        copyright = "Copyright 2018, Zhang Shaowei<z_sw@hotmail.com>"
        )
]

setup(name='Subsidy Collector',
      version = '0.0.1',
      description = 'Utility for T&C team collecting and managing subsidy',
      options = dict(build_exe = buildOptions),
      executables = executables)
