# -*- coding: utf-8 -*-
import xlrd, calendar
from pathlib import Path

def collect_data(pathname, monthname):
    data = []
    for filename in Path(pathname).glob("*.xlsm"):
        user = collect_user(str(filename), monthname)
        if user:
            data.append(user)

    return data

def collect_user(filename, monthname):
    year = monthname[0:4]
    month = monthname[4:6].lstrip('0')
    days = calendar.monthrange(int(year), int(month))[1]

    user = {}
    xlsx = xlrd.open_workbook(filename)
    if not monthname in xlsx.sheet_names():
        print("Warning: can not find data in {} during {}."
                .format(filename, monthname))
        return user
    
    sheet = xlsx.sheet_by_name(monthname)
    if sheet.ncols < 4:
        return user

    user['user'] = sheet.cell(0, 0).value
    user['id'] = sheet.cell(1, 0).value
    user['prjs'] = {}
    user['memo'] = sheet.col_values(sheet.ncols - 1, 2, days * 2 + 2)

    # exclude the description column
    for iprj in range(2, sheet.ncols - 1):
        if not sheet.cell(1, iprj).value:
            continue

        user['prjs'][sheet.cell(1, iprj).value] = sheet.col_values(
                iprj, 2, days * 2 + 2)

    return user

