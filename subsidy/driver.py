# -*- coding: utf-8 -*-
from os import path
from subsidy import collector, output, template

def print_data(data):
    for user in data:
        if user['prjs'].keys():
            print(user['user'], user['id'])
        for p in user['prjs'].keys():
            print(' ', p)
            for j in range(len(user['prjs'][p])):
                print('  ', user['prjs'][p][j], ': ', user['memo'][j])

def get_subsidy(outputdir, inputdir, monthname, project, merging):
    projects = [project]
    projects.extend(merging)
    data = collector.collect_data(inputdir, monthname)
    for u in range(len(data)):
        for p in data[u]['prjs']:
            if not [d for d in data[u]['prjs'][p] if d]:
                data[u]['prjs'][p] = []

        data[u]['prjs'] = {k:v
                for k, v in data[u]['prjs'].items()
                if k in projects and v}

        if not data[u]['prjs']:
            print('Warning: can not find {} information for {}.'
                    .format(','.join(projects), data[u]['user'])) 
    
    output.write_xlsx(data, path.join(outputdir, '{}_{}.xlsx'.format(project, monthname)),
            monthname, project) 

def get_template(outputdir):
    template.write_template(outputdir)
