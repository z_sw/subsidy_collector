# -*- coding: utf-8 -*-
import datetime, xlsxwriter
from os import path

def write_template(outdir):
    workbook = xlsxwriter.Workbook(path.join(outdir, '工时统计_NAME_YYYYMM.xlsm'))
    workbook.add_vba_project('./vbaProject.bin')

    day_format = workbook.add_format({'bottom': 1, 'top': 6, 'left': 1,
        'right': 1, 'font_size': 11, 'align': 'center', 'valign': 'vcenter',
        'fg_color': '#DCE6F1', 'locked': 0})

    night_format = workbook.add_format({'bottom': 6, 'top': 1, 'left': 1,
        'right': 1, 'font_size': 11, 'align': 'center', 'valign': 'vcenter',
        'fg_color': '#BFBFBF', 'locked': 0})

    day_l_format = workbook.add_format({'bottom': 1, 'top': 6, 'left': 6,
        'right': 1, 'font_size': 11, 'align': 'center', 'valign': 'vcenter',
        'fg_color': '#DCE6F1', 'locked': 0})

    night_l_format = workbook.add_format({'bottom': 6, 'top': 1, 'left': 6,
        'right': 1, 'font_size': 11, 'align': 'center', 'valign': 'vcenter',
        'fg_color': '#BFBFBF', 'locked': 0})

    day_title_format = workbook.add_format({'bottom': 1, 'top': 6, 'left': 1,
        'right': 1, 'font_size': 11, 'align': 'center', 'valign': 'vcenter',
        'fg_color': '#DCE6F1'})

    night_title_format = workbook.add_format({'bottom': 6, 'top': 1, 'left': 1,
        'right': 1, 'font_size': 11, 'align': 'center', 'valign': 'vcenter',
        'fg_color': '#BFBFBF'})

    desc_day_format = workbook.add_format({'bottom': 1, 'top': 6, 'left': 6,
        'right': 1, 'font_size': 11, 'align': 'left', 'valign': 'top',
        'fg_color': '#DCE6F1', 'locked': 0, 'text_wrap': True})

    desc_night_format = workbook.add_format({'bottom': 6, 'top': 1, 'left': 6,
        'right': 1, 'font_size': 11, 'align': 'left', 'valign': 'top',
        'fg_color': '#BFBFBF', 'locked': 0, 'text_wrap': True})

    project_format = workbook.add_format({'bottom': 6, 'top': 1, 'left': 1,
        'right': 1, 'font_size': 11, 'align': 'center', 'valign': 'vcenter',
        'fg_color': '#FCD5B4', 'locked': 0})

    project_l_format = workbook.add_format({'bottom': 6, 'top': 1, 'left': 6,
        'right': 1, 'font_size': 11, 'align': 'center', 'valign': 'vcenter',
        'fg_color': '#FCD5B4', 'locked': 0})

    head_format = workbook.add_format({'bottom': 1, 'top': 6, 'left': 6,
        'right': 1, 'font_size': 11, 'align': 'center', 'valign': 'vcenter',
        'fg_color': '#FCD5B4'})

    title_format = workbook.add_format({'bottom': 6, 'top': 6, 'left': 1,
        'right': 1, 'font_size': 11, 'align': 'center', 'valign': 'vcenter',
        'fg_color': '#8DB4E2'})

    user_format = workbook.add_format({'bottom': 1, 'top': 6, 'left': 6,
        'right': 1, 'font_size': 11, 'align': 'center', 'valign': 'vcenter',
        'fg_color': '#FCD5B4'})

    id_format = workbook.add_format({'bottom': 1, 'top': 1, 'left': 6,
        'right': 1, 'font_size': 11, 'align': 'center', 'valign': 'vcenter',
        'fg_color': '#FCD5B4'})

    notice_format = workbook.add_format({'bottom': 0, 'top': 0, 'left': 0,
        'right': 0, 'font_size': 10, 'align': 'left', 'valign': 'vcenter'})

    input_format = workbook.add_format({'bottom': 1, 'top': 1, 'left': 1,
        'right': 1, 'font_size': 11, 'align': 'left', 'valign': 'vcenter',
        'font_color': '#FF0000', 'locked': 0})

    # The main sheet
    worksheet = workbook.add_worksheet('主页')
    worksheet.set_column(0, 0, 4.00)
    worksheet.set_column(1, 1, 15.00)
    worksheet.set_column(2, 2, 15.00)
    worksheet.set_column(3, 3, 30.00)
    worksheet.set_column(4, 4, 4.00)

    worksheet.merge_range(1, 1, 1, 3, '说明', head_format)
    s = '本表格用于自动统计安装调试工时，也可用来作为工作日志记录。'
    worksheet.merge_range(2, 1, 2, 3, s, notice_format)
    s = '请先填写基本信息，其中项目中输入以空格分隔的项目缩写名称，按"生成/修改"'
    worksheet.merge_range(3, 1, 3, 3, s, notice_format)
    s = '生成、修改后转到工作表。在工作表中可直接编辑或删除工作表中的项目名称；'
    worksheet.merge_range(4, 1, 4, 3, s, notice_format)
    s = '再次按"主页"上的按钮时，会把输入的但工作表中不存在的项目加入工作表，并删除'
    worksheet.merge_range(5, 1, 5, 3, s, notice_format)
    s = '工作表中第二个项目后且项目名称为空的列。'
    worksheet.merge_range(6, 1, 6, 3, s, notice_format)
    s = '对应项目列中，仅能填入数字"1"，或删除内容，表示工时所属项目。'
    worksheet.merge_range(7, 1, 7, 3, s, notice_format)
    s = '无项目名称、重复统计或无工作内容的工时会被忽略。'
    worksheet.merge_range(8, 1, 8, 3, s, notice_format)
    s = '若有问题请联系Zhang Shaowei<z_sw@hotmail.com>。'
    worksheet.merge_range(9, 1, 9, 3, s, notice_format)

    worksheet.merge_range(12, 1, 12, 3, '基本信息', head_format)
    worksheet.write_string(13, 2, '姓名', title_format)
    worksheet.write_string(13, 3, '', input_format)
    worksheet.write_string(14, 2, '工号', title_format)
    worksheet.write_string(14, 3, '', input_format)
    worksheet.write_string(15, 2, '年度', title_format)
    worksheet.write_formula(15, 3, '=YEAR(TODAY())', input_format)
    worksheet.data_validation(15, 3, 15, 3, {
        'validate': 'list', 'source': '=$A$20:$C$20',
        'ignore_blank': False, 'error_title': '无效输入!',
        'error_message': '输入或选择有效年度'})

    worksheet.write_string(16, 2, '月份', title_format)
    worksheet.write_formula(16, 3, '=MONTH(TODAY())', input_format)
    worksheet.data_validation(16, 3, 16, 3, {'validate': 'list',
        'source': [str(i) for i in range(1,13)],
        'ignore_blank': False, 'error_title': '无效输入!',
        'error_message': '请输入或选择有效月份'})

    worksheet.write_string(17, 2, '项目', title_format)
    worksheet.write_string(17, 3, '', input_format)
    worksheet.insert_button(20, 3, {'macro': 'new_sheet',
        'caption': '生成/修改', 'width': 65, 'heigh': 30,
        'x_offset': 150})

    # Year List Validation, From the last year to the next year.
    worksheet.write_formula(19, 0, '=YEAR(TODAY())-1')
    worksheet.write_formula(19, 1, '=YEAR(TODAY())')
    worksheet.write_formula(19, 2, '=YEAR(TODAY())+1')
    worksheet.set_row(19, None, None, {'hidden': True})
    worksheet.insert_image(14, 1, 'logo.png',
            {'x_offset': 2, 'y_offset': 2, 'x_scale': 1.0, 'y_scale': 1.0})
    worksheet.protect()

    # The template sheet
    worksheet = workbook.add_worksheet('_')
    worksheet.set_column(0, 0, 3.00)
    worksheet.set_column(1, 1, 4.63)
    worksheet.set_column(2, 3, 5.63)
    worksheet.set_column(4, 4, 50)
    
    worksheet.merge_range(0, 0, 0, 1, '[姓名]', user_format)
    worksheet.merge_range(0, 2, 0, 3, '项目', head_format)
    worksheet.write_string(0, 4, '[YYYY]年[MM]月', user_format)
    worksheet.merge_range(1, 0, 1, 1, '[工号]', id_format)

    for r in range(31):
        worksheet.merge_range(2 + 2 * r, 0, 3 + 2 * r, 0, r + 1, title_format)
        worksheet.write_string(2 + 2 * r, 1, '白班', day_title_format)
        worksheet.write_string(3 + 2 * r, 1, '夜班', night_title_format)

    v_r = 2
    worksheet.write_string(1, 2, '', project_l_format)
    worksheet.write_string(1, 3, '', project_format)
    for r in range(31):
        worksheet.write_string(2 + 2 * r, 2, '', day_l_format)
        worksheet.write_string(2 + 2 * r, 3, '', day_format)
        worksheet.write_string(3 + 2 * r, 2, '', night_l_format)
        worksheet.write_string(3 + 2 * r, 3, '', night_format)

    worksheet.write_string(1, 4, '工作内容', id_format)
    for r in range(31):
        worksheet.write_string(2 + 2 * r, 4, '', desc_day_format)
        worksheet.write_string(3 + 2 * r, 4, '', desc_night_format)

    worksheet.freeze_panes(2, 2)

    # Data validation
    worksheet.data_validation(2, 2, 65, 3, {
        'validate': 'integer', 'criteria': '==', 'value': 1,
        'ignore_blank': True, 'error_title': '无效输入!',
        'error_message': '只能输入1'})
    worksheet.protect()
    worksheet.hide()

    workbook.close()

