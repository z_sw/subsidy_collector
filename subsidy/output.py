# -*- coding: utf-8 -*-
import calendar, xlsxwriter

def write_xlsx(data, pathname, monthname, projectname):
    workbook = xlsxwriter.Workbook(pathname)
    write_particular(workbook, data, monthname, projectname)
    wirte_outline(workbook, data, monthname, projectname)
    workbook.close()

def write_particular(workbook, data, monthname, projectname):
    year = monthname[0:4]
    month = monthname[4:6].lstrip('0')
    days = calendar.monthrange(int(year), int(month))[1]

    worksheet = workbook.add_worksheet('Particular')

    worksheet.set_column(0, 0, 3.00)
    worksheet.set_column(1, 1, 4.63)
    
    head_top_format = workbook.add_format({
        'bold': True, 'bottom': 1, 'top': 6, 'left': 1, 'right': 1, 'font_size': 11,
        'align': 'center', 'valign': 'vcenter', 'fg_color': '#8DB4E2'})

    head_middle_format = workbook.add_format({
        'bottom': 1, 'top': 1, 'left': 1, 'right': 1, 'font_size': 11,
        'align': 'center', 'valign': 'vcenter', 'fg_color': '#8DB4E2'})

    head_bottom_format = workbook.add_format({
        'bold': True, 'bottom': 6, 'top': 1, 'left': 1, 'right': 1, 'font_size': 11,
        'align': 'center', 'valign': 'vcenter', 'fg_color': '#8DB4E2'})

    user_format = workbook.add_format({
        'bottom': 1, 'top': 6, 'left': 6, 'right': 1, 'font_size': 11,
        'align': 'center', 'valign': 'vcenter', 'fg_color': '#92D050'})

    id_format = workbook.add_format({
        'bottom': 1, 'top': 1, 'left': 6, 'right': 1, 'font_size': 11,
        'align': 'center', 'valign': 'vcenter', 'fg_color': '#92D050'})

    project_format = workbook.add_format({
        'bottom': 6, 'top': 1, 'left': 1, 'right': 1, 'font_size': 11,
        'align': 'center', 'valign': 'vcenter', 'fg_color': '#92D050'})

    project_l_format = workbook.add_format({
        'bottom': 6, 'top': 1, 'left': 6, 'right': 1, 'font_size': 11,
        'align': 'center', 'valign': 'vcenter', 'fg_color': '#92D050'})

    merge_format = workbook.add_format({
        'bold': True, 'bottom': 6, 'top': 6, 'left': 1, 'right': 1, 'font_size': 11,
        'align': 'center', 'valign': 'vcenter', 'fg_color': '#8DB4E2'})

    day_format = workbook.add_format({
        'bottom': 1, 'top': 6, 'left': 1, 'right': 1, 'font_size': 11,
        'align': 'center', 'valign': 'vcenter', 'fg_color': '#DCE6F1'})

    day_l_format = workbook.add_format({
        'bottom': 1, 'top': 6, 'left': 6, 'right': 1, 'font_size': 11,
        'align': 'center', 'valign': 'vcenter', 'fg_color': '#DCE6F1'})

    night_format = workbook.add_format({
        'bottom': 6, 'top': 1, 'left': 1, 'right': 1, 'font_size': 11,
        'align': 'center', 'valign': 'vcenter', 'fg_color': '#BFBFBF'})

    night_l_format = workbook.add_format({
        'bottom': 6, 'top': 1, 'left': 6, 'right': 1, 'font_size': 11,
        'align': 'center', 'valign': 'vcenter', 'fg_color': '#BFBFBF'})

    worksheet.merge_range(0, 0, 0, 1, '统计周期', head_top_format)
    worksheet.merge_range(1, 0, 1, 1, monthname, head_middle_format)
    worksheet.write_string(2, 0, '日', head_bottom_format)
    worksheet.write_string(2, 1, '班次', head_bottom_format)

    for r in range(days):
        worksheet.merge_range(3 + 2 * r, 0, 4 + 2 * r, 0, r + 1, merge_format)
        worksheet.write_string(3 + 2 * r, 1, '白班', day_format)
        worksheet.write_string(4 + 2 * r, 1, '夜班', night_format)
        
    start_col = 2
    for user in data:
        merge_user_col = len(user['prjs'])

        if merge_user_col == 0:
            continue

        worksheet.set_column(start_col, start_col + merge_user_col - 1, 5.5)

        if merge_user_col > 1:
            worksheet.merge_range(0, start_col, 0, start_col + merge_user_col - 1,
                    user['user'], user_format)
            worksheet.merge_range(1, start_col, 1, start_col + merge_user_col - 1,
                    str(int(user['id'])), id_format)
        else:
            worksheet.write_string(0, start_col, user['user'], user_format)
            worksheet.write_string(1, start_col, str(int(user['id'])), id_format)

        prj_col = start_col
        is_left = False
        work_day_format = day_format;
        work_night_format = night_format;
        work_project_format = project_format
        for prj in user['prjs'].keys():
            if prj_col == start_col:
                work_day_format = day_l_format
                work_night_format = night_l_format
                work_project_format = project_l_format
            else:
                work_day_format = day_format
                work_night_format = night_format
                work_project_format = project_format

            worksheet.write_string(2, prj_col, prj, work_project_format)

            for i in range(days):
                if user['prjs'][prj][2 * i]:
                    if user['memo'][2 * i]:
                        worksheet.write_number(2 * i + 3, prj_col,
                                int(user['prjs'][prj][2 * i]), work_day_format)
                        worksheet.write_comment(2 * i + 3, prj_col, user['memo'][2 * i])
                    else:
                        worksheet.write_string(2 * i + 3, prj_col, '', work_day_format)
                else:
                    worksheet.write_string(2 * i + 3, prj_col, '', work_day_format)

                if user['prjs'][prj][2 * i + 1]:
                    if user['memo'][2 * i + 1]:
                        worksheet.write_number(2 * i + 4, prj_col,
                                int(user['prjs'][prj][2 * i + 1]), work_night_format)
                        worksheet.write_comment(2 * i + 4, prj_col, user['memo'][2 * i + 1])
                    else:
                        worksheet.write_string(2 * i + 4, prj_col, '',
                                work_night_format)
                else:
                    worksheet.write_string(2 * i + 4, prj_col, '', work_night_format)

            prj_col += 1

        start_col += merge_user_col

    worksheet.freeze_panes(3, 2)
    worksheet.protect()

def wirte_outline(workbook, data, monthname, projectname):
    year = monthname[0:4]
    month = monthname[4:6].lstrip('0')
    days = calendar.monthrange(int(year), int(month))[1]

    worksheet = workbook.add_worksheet(monthname)
    worksheet.set_column(0, 0, 7.13)
    worksheet.set_column(1, 1, 4.63)
    worksheet.set_column(2, 2 + days, 2.13)
    worksheet.set_column(2 + days, 3 + days, 4.63)

    project_format = workbook.add_format({
        'bold': True, 'bottom': 6, 'top': 1, 'left': 1, 'right': 1, 'font_size': 11,
        'align': 'center', 'valign': 'vcenter', 'fg_color': '#8DB4E2'})

    head_format = workbook.add_format({
        'bottom': 6, 'top': 6, 'left': 1, 'right': 1, 'font_size': 11,
        'align': 'center', 'valign': 'vcenter', 'fg_color': '#8DB4E2'})

    user_name_format = workbook.add_format({
        'bold': True, 'bottom': 0, 'top': 6, 'left': 1, 'right': 1, 'font_size': 11,
        'align': 'center', 'valign': 'bottom', 'fg_color': '#8DB4E2'})

    user_id_format = workbook.add_format({
        'bold': True, 'bottom': 6, 'top': 0, 'left': 1, 'right': 1, 'font_size': 11,
        'align': 'center', 'valign': 'top', 'fg_color': '#8DB4E2'})

    day_format = workbook.add_format({
        'bottom': 1, 'top': 6, 'left': 1, 'right': 1, 'font_size': 11,
        'align': 'center', 'valign': 'vcenter', 'fg_color': '#DCE6F1'})

    night_format = workbook.add_format({
        'bottom': 6, 'top': 1, 'left': 1, 'right': 1, 'font_size': 11,
        'align': 'center', 'valign': 'vcenter', 'fg_color': '#BFBFBF'})

    day_l_format = workbook.add_format({
        'bottom': 1, 'top': 6, 'left': 6, 'right': 1, 'font_size': 11,
        'align': 'center', 'valign': 'vcenter', 'fg_color': '#DCE6F1'})

    night_l_format = workbook.add_format({
        'bottom': 6, 'top': 1, 'left': 6, 'right': 1, 'font_size': 11,
        'align': 'center', 'valign': 'vcenter', 'fg_color': '#BFBFBF'})

    worksheet.merge_range(0, 0, 1, 1, '', project_format)
    worksheet.insert_image(0, 0, 'logo.png',
            {'x_offset': 2, 'y_offset': 2, 'x_scale': 1.0, 'y_scale': 1.0})

    worksheet.merge_range(0, 2, 0, 2 + days, projectname + '_' + monthname, project_format)

    r = 2
    for user in data:
        prj_data = []
        for prj in user['prjs'].values():
            if not prj_data:
                prj_data = prj
                for i in range(days):
                    worksheet.write_number(1, i + 2, i + 1, head_format)
                worksheet.write_string(1, days + 2, '合计', head_format)
            else:
                for i in range(2 * days):
                    if not prj_data[i]:
                        prj_data[i] = prj[i]

        if not prj_data:
            continue

        worksheet.write_string(r, 0, user['user'], user_name_format)
        worksheet.write_string(r + 1, 0, str(int(user['id'])), user_id_format)

        worksheet.write_string(r, 1, '白班', day_format)
        worksheet.write_string(r + 1, 1, '夜班', night_format)

        day_count = 0
        night_count = 0
        for i in range(days):
            v1 = ''
            v2 = ''
            if prj_data[i * 2]:
                v1 = str(int(prj_data[i * 2]))
                day_count += 1
            if prj_data[i * 2 + 1]:
                v2 = str(int(prj_data[i * 2 + 1]))
                night_count += 1

            if 0 == i:
                worksheet.write_string(r, i + 2, v1, day_l_format)
                worksheet.write_string(r + 1, i + 2, v2, night_l_format)
            else:
                worksheet.write_string(r, i + 2, v1, day_format)
                worksheet.write_string(r + 1, i + 2, v2, night_format)

        day_val = ''
        night_val = ''

        if day_count:
            day_val = str(int(day_count))

        if night_count:
            night_val = str(int(night_count))

        worksheet.write_string(r, days + 2, day_val, day_l_format)
        worksheet.write_string(r + 1, days + 2, night_val, night_l_format)

        r += 2

    worksheet.freeze_panes(2, 2)
    worksheet.protect()

