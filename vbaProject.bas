Sub new_sheet()
    If Len(Range("D14").Value) = 0 Or Len(Range("D15").Value) = 0 Or Len(Range("D16").Value) = 0 Or Len(Range("D17").Value) = 0 Then
        Dim Missing As String
        If Len(Range("D14").Value) = 0 Then
            Missing = Missing & " 姓名"
        End If
        If Len(Range("D15").Value) = 0 Then
            Missing = Missing & " 工号"
        End If
        If Len(Range("D16").Value) = 0 Then
            Missing = Missing & " 年度"
        End If
        If Len(Range("D17").Value) = 0 Then
            Missing = Missing & " 月份"
        End If
        MsgBox "请填写" & Missing, vbOKOnly, "提示"
    Else
        copy_sheet "_", Range("D16").Value & Format(Range("D17").Value, "00")
        ActiveWorkbook.Save
    End If
End Sub

Function copy_sheet(oldS As String, newS As String)
    Dim curS As String
    Dim Prjs As Variant
    Dim nPrj As Integer
    Dim e As Boolean
    Dim c As Range
    Dim Prj As Variant
    Dim i As Integer

    curS = ActiveSheet.Name
    If Not sheet_exists(newS) Then
        If Len(Range("D18").Value) = 0 Then
            MsgBox "请填写 项目", vbOKOnly, "提示"
            Exit Function
        Else
            Sheets(oldS).Copy After:=Sheets(curS)
            Sheets(oldS + " (2)").Name = newS
            Sheets(newS).Visible = True
            Sheets(newS).Unprotect
            Sheets(newS).Range("A1").Value = Sheets(curS).Range("D14").Value
            Sheets(newS).Range("A2").Value = Sheets(curS).Range("D15").Value
            Sheets(newS).Range("E1").Value = Sheets(curS).Range("D16").Value & "年" & Format(Sheets(curS).Range("D17").Value, "00") & "月"
        End If
    Else
        Sheets(newS).Unprotect
    End If
    
    Prjs = Split(Sheets(curS).Range("D18").Value, " ")
    If Sheets(newS).Range("C1").MergeCells Then
        For Each Prj In Prjs
            Prj = Trim(Prj)
            If Len(Prj) <> 0 Then
                e = False
                nPrj = Sheets(newS).Range("C1").MergeArea.Columns.Count
                For Each c In Sheets(newS).Range(Sheets(newS).Cells(2, 3), Sheets(newS).Cells(2, 2 + nPrj))
                    If Trim(c.Value) = Prj Then
                        e = True
                    End If
                Next
                If Not e Then
                    For Each c In Sheets(newS).Range(Sheets(newS).Cells(2, 3), Sheets(newS).Cells(2, 2 + nPrj))
                        If Len(c.Value) = 0 And Not e Then
                            c.Value = Prj
                            e = True
                        End If
                    Next
                    
                    If Not e Then
                        Sheets(newS).Columns(4).Copy
                        Sheets(newS).Columns(3 + nPrj).Insert Shift:=xlToRight
                        Sheets(newS).Cells(2, 3 + nPrj).Value = Prj
                        Sheets(newS).Range(Sheets(newS).Cells(3, 3 + nPrj), Sheets(newS).Cells(64, 3 + nPrj)).Value = ""
                        Sheets(newS).Range(Sheets(newS).Cells(1, 3), Sheets(newS).Cells(1, 3 + nPrj)).Merge
                        With Sheets(newS).Range(Sheets(newS).Cells(2, 3 + nPrj), Sheets(newS).Cells(64, 3 + nPrj)).Borders(xlEdgeLeft)
                            .LineStyle = xlContinuous
                            .Weight = xlThin
                        End With
                    End If
                End If
            End If
        Next
        
        nPrj = Sheets(newS).Range("C1").MergeArea.Columns.Count
        i = 3
        Do While i <= nPrj
            e = False
            Do While Len(Sheets(newS).Cells(2, i + 2).Value) = 0 And i <= nPrj
                Sheets(newS).Columns(i + 2).Delete
                e = True
            Loop
            If Not e Then
               i = i + 1
            End If
            nPrj = Sheets(newS).Range("C1").MergeArea.Columns.Count
        Loop
    End If
    Sheets(newS).Protect
    Sheets(newS).Activate
End Function

Function sheet_exists(sheetToFind As String) As Boolean
    sheet_exists = False
    For Each Sheet In Worksheets
        If sheetToFind = Sheet.Name Then
            sheet_exists = True
            Sheet.Activate
            Exit Function
        End If
    Next
End Function

